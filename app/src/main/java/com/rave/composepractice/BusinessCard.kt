package com.rave.composepractice

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rave.composepractice.ui.theme.ComposePracticeTheme

@Composable
fun BusinessCardScreen() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .background(Color.Gray),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        val image = painterResource(R.drawable.android_logo)
        Image(painter = image, contentDescription = null)
        Text(
            text = "Mark Dawes",
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            modifier = Modifier.padding(top = 24.dp, bottom = 8.dp).background(Color.Gray),
            // color = Color(0xFFFFFF)
        )
        Text(
            text = "Android Developer Extraordinaire",
            fontSize = 16.sp,
            // color = Color(0xFFFFFF)
        )
        Text(
            text = "Phone: (609)-578-0381",
            fontSize = 16.sp,
            // color = Color(0xFFFFFF)
        )
        Text(
            text = "Email: mrkdawes8@gmail",
            fontSize = 16.sp,
            // color = Color(0xFFFFFF)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun BusinessCardPreview() {
    ComposePracticeTheme {
        Surface {
            BusinessCardScreen()
        }
    }
}